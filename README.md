# Welcome to the Open Global Mind Agora!
This is an [Agora](https://flancia.org/agora). You can find the reference implementation live at <https://anagora.org/>. This repository maps to the Open Global Mind Agora which should be live at https://ogm.agor.ai.

# Wait, what's an Agora again?

An [Agora](https://anagora.org/wiki/Agora) is a distributed, goal-oriented social network operating on a cooperatively built and maintained knowledge graph. The implementation you are currently looking at tries to assemble such a graph out of a collection of [digital gardens](flancia.org/go/garden), but other data sources are coming.

You can view the reference Agora at <https://anagora.org>. For how to write to it: if you take personal digital notes with some system such as [foam](https://anagora.org/foam) or [obsidian](https://anagora.org/obsidian), you are most of the way there; all you need to do is share them with the Agora (see "join" above). If you don't, but you would like to, please refer to [agora client](https://anagora.org/agora-join) or reach out!

# To join

If you would like to join this Agora, please send a PR adding your garden or wiki to `sources.yaml` or reach out to an existing user.

After being integrated, your repository will appear live at <https://ogm.agor.ai/@username> and supported notes and other resources will be surfaced in the Agora in relevant nodes; this means that if you volunteer a note named ```foo.md```, it will show up in node <https://anagora.org/foo> together with all similarly named notes by other Agora users.

# Contract
***If you contribute directly to an Agora you are assumed to be in agreement with its then current contract.*** 

Please refer to the Agora's [contract](https://ogm.agor.ai/contract), in particular as posted by the system account @agora (which is binding for all users).
